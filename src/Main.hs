module Main where

import Text.ParserCombinators.Parsec hiding (spaces)
import System.Environment
import Control.Monad
import Numeric (readInt, readOct, readHex, readDec)
import Data.Char (digitToInt)

data LispVal = Atom String
             | List [LispVal]
             | DottedList [LispVal] LispVal
             | Number Integer
             | String String
             | Bool Bool

instance Eq LispVal where
  Atom x == Atom y = x == y
  List xs == List ys = xs == ys
  Number x == Number y = x == y
  String x == String y = x == y
  Bool x == Bool y = x == y
  DottedList xs x == DottedList ys y = (xs == ys) && x == y
  _ == _ = False

spaces :: Parser ()
spaces = skipMany1 space

symbol :: Parser Char
symbol = oneOf "!#$%&|*+-/:<=>?@^_~"

escapeChar :: Parser Char
escapeChar = char '\\' >> oneOf "\"nrt\\"

parseString :: Parser LispVal
parseString = do
  char '"'
  x <- many (noneOf "\"" <|> escapeChar)
  char '"'
  return $ String x

parseAtom :: Parser LispVal
parseAtom = do
  first <- letter <|> symbol
  rest <- many (letter <|> digit <|> symbol)
  let atom = first:rest
  return $ case atom of
    "#t" -> Bool True
    "#f" -> Bool False
    _    -> Atom atom

parseOctal :: Parser LispVal
parseOctal = do
  x <- string "#o" >> many1 digit
  return $ Number . fst . head $ readOct x

parseHex :: Parser LispVal
parseHex = do
  x <- string "#x" >> many1 digit
  return $ Number . fst . head $ readHex x

parseBinary :: Parser LispVal
parseBinary = do
  x <- string "#b" >> many1 (oneOf "01")
  return $ Number . fst . head $ readInt 2 (`elem` "01") digitToInt x

parseDecimal :: Parser LispVal
parseDecimal = do
  x <- optional (string "#d") >> many1 digit
  return $ Number . fst . head $ readDec x

parseNumber :: Parser LispVal
parseNumber = parseOctal
              <|> parseHex
              <|> parseBinary
              <|> parseDecimal

parseExpr :: Parser LispVal
parseExpr = parseAtom
            <|> parseString
            <|> parseNumber

readExpr :: String -> String
readExpr input = case parse parseExpr "lisp" input of
  Left err -> "No match: " ++ show err
  Right val -> "Found value"

testExpr :: String -> LispVal -> String
testExpr input expected = case parse parseExpr "lisp" input of
  Left err -> "No match: " ++ show err
  Right val -> if val == expected then
                 "Matches expected AST"
               else "Does not match the expected AST"

main :: IO ()
main = do
  (expr:_) <- getArgs
  putStrLn (readExpr expr)
